<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/auth/get', ['uses' => 'AuthController@index']);
Route::post('/auth/set', ['uses' => 'AuthController@setUser']);

Route::get('/{path?}', [
    'uses' => 'ReactController@show',
    'as' => 'react',
    'where' => ['path' => '.*']
]);

Route::get('/', [
    'uses' => 'ReactController@show',
    'as' => 'react',
]);

Route::get('/category/popular', [
    'uses' => 'ReactController@show',
    'as' => 'react',
]);

Route::get('/category/now_playing', [
    'uses' => 'ReactController@show',
    'as' => 'react',
]);

Route::get('/category/upcoming', [
    'uses' => 'ReactController@show',
    'as' => 'react',
]);

Route::get('/register', [
    'uses' => 'ReactController@show',
    'as' => 'react',
]);

Auth::routes();


