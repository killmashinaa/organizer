const filmsPopularRequested = () => {
    return {
        type: 'FETCH_POPULAR_FILMS_REQUEST'
    };
};

const filmsPopularLoaded = (newPopularFilms) => {
    return {
        type: 'FETCH_POPULAR_FILMS_SUCCESS',
        payload: newPopularFilms
    };
};

const filmsPopularError = (error) => {
    return {
        type: 'FETCH_POPULAR_FILMS_FAILURE',
        payload: error
    };
};

const fetchPopularFilms = (datastoreService) => () => (dispatch) => {
    dispatch(filmsPopularRequested());
    datastoreService.getPopularFilms()
        .then((data) => dispatch(filmsPopularLoaded(data)))
        .catch((err) => dispatch(filmsPopularError(err)));
};

const addUserCheckAuth = (dataUser) => {
    return {
        type: 'ADD_USER_CHECK_AUTH',
        payload: dataUser
    }
};

const removeUserCheckAuth = (dataUser) => {
    return {
        type: 'REMOVE_USER_CHECK_AUTH',
        payload: dataUser
    }
};
export {
    fetchPopularFilms,
    addUserCheckAuth,
    removeUserCheckAuth
};
