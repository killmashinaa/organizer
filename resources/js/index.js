/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */
require('./bootstrap');
/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import ErrorBoundry from './components/error-boundry';
import DatastoreService from  './services/datastore-service';
import { DatastoreServiceProvider } from './components/datastore-service-provider';


import App from './components/app';

import store from './store';

const datastoreService = new DatastoreService();
ReactDOM.render(
    <Provider store={store}>
        <ErrorBoundry>
            <DatastoreServiceProvider value={datastoreService}>
            <Router>
                <App/>
            </Router>
            </DatastoreServiceProvider>
        </ErrorBoundry>
    </Provider>,
  document.getElementById('root')
);
