import notFoundImage from '../image/404.png';

export default class DatastoreService {

    data = [
        {
            id: 1,
            title: 'Production-Ready Microservices',
            author: 'Susan J. Fowler',
            price: 32,
            coverImage: 'https://images-na.ssl-images-amazon.com/images/I/41yJ75gpV-L._SX381_BO1,204,203,200_.jpg'},
        {
            id: 2,
            title: 'Release It!',
            author: 'Michael T. Nygard',
            price: 45,
            coverImage: 'https://images-na.ssl-images-amazon.com/images/I/414CRjLjwgL._SX403_BO1,204,203,200_.jpg'}
    ];

    _apiBase = 'https://api.themoviedb.org';

    getResource = async (url, param = '') => {
        const res = await fetch(`${this._apiBase}${url}?api_key=cc9ec56bfe1118abfe979f5357d5a211${param}`);

        if (!res.ok) {
            throw new Error(`Could not fetch ${url}` +
                `, received ${res.status}`)
        }
        return await res.json();
    };

    getAllFilmsWithJackReacher = async () => {
        const res = await this.getResource(`/3/search/movie`, '&query=Jack+Reacher');
        return res.results;
    };

    getImages = async () => {
        const res = await this.getResource(`/3/movie/550-fight-club/images`);
        const new_res = res.backdrops.map(function(item) {
            item.file_path = 'https://image.tmdb.org/t/p/w500/' + item.file_path;
            return item;
        });
        return new_res;
    };

    getPopularFilmsAsync = async (page = 1, FilmsFromPage = []) => {
        const res = await this.getResource(`/3/movie/popular`, `&page=${page}&language=ru-Ru`);
        res.results.map(function(item) {
            let notImage = false;
            if (item.backdrop_path == null) {
                item.backdrop_path = notFoundImage;
                notImage = true;
            }
            if (notImage) {
                return item;
            }

            item.poster_path = 'https://image.tmdb.org/t/p/w500' + item.poster_path;
            item.backdrop_path = 'https://image.tmdb.org/t/p/w500' + item.backdrop_path;
            return item;
        });


        if (FilmsFromPage.results !== undefined) {
            res.results = [...FilmsFromPage.results, ...res.results]
        }
        return res;
    };

    getPopularFilms() {
        return new Promise((resolve, reject) => {
            resolve(this.getPopularFilmsAsync());
        });
    };

    getNowPlayingFilms = async (page = 1, FilmsFromPage = []) => {
        const res = await this.getResource(`/3/movie/now_playing`, `&page=${page}&language=ru-Ru&region=RU`);
        res.results.map(function(item) {
            let notImage = false;
            if (item.backdrop_path == null) {
                item.backdrop_path = notFoundImage;
                notImage = true;
            }
            if (notImage) {
                return item;
            }

            item.poster_path = 'https://image.tmdb.org/t/p/w500' + item.poster_path;
            item.backdrop_path = 'https://image.tmdb.org/t/p/w500' + item.backdrop_path;
            return item;
        });


        if (FilmsFromPage.results !== undefined) {
            res.results = [...FilmsFromPage.results, ...res.results]
        }
        return res;
    };

    getUpcomingFilms = async (page = 1, FilmsFromPage = []) => {
        const res = await this.getResource(`/3/movie/upcoming`, `&page=${page}&language=ru-Ru&region=RU`);
        res.results.map(function(item) {
            let notImage = false;
            if (item.backdrop_path == null) {
                item.backdrop_path = notFoundImage;
                notImage = true;
            }
            if (notImage) {
                return item;
            }

            item.poster_path = 'https://image.tmdb.org/t/p/w500' + item.poster_path;
            item.backdrop_path = 'https://image.tmdb.org/t/p/w500' + item.backdrop_path;
            return item;
        });


        if (FilmsFromPage.results !== undefined) {
            res.results = [...FilmsFromPage.results, ...res.results]
        }
        return res;
    };

    getVideoForFilm = async (id = null) => {
        const res = await this.getResource(`/3/movie/${id}/videos`,'&language=en-US');
        res.results.map(function(item, index) {
            if (index >= 3) {
                return false;
            }
            if (item.site == "YouTube") {
                item.video_url = 'https://www.youtube.com/embed/' + item.key;
                return item;
            }
        });

        res.results = res.results.filter((el) => {
            if (el.video_url !== undefined) {
                return true;
            }
        });


        return res.results;

    }



}
