export default class DatastoreAuth {
    _apiBase = 'organizer.loc';

    getResource = async (url, param = '') => {
        const res = await fetch(`${url}?${param}`);

        if (!res.ok) {
            throw new Error(`Could not fetch ${url}` +
                `, received ${res.status}`)
        }
        return await res.json();
    };

    setDataResource = async (url = '', data = {}) => {
        // Значения по умолчанию обозначены знаком *
        return await fetch(url, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, cors, *same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json',
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrer: 'no-referrer', // no-referrer, *client
            body: JSON.stringify(data), // тип данных в body должен соответвовать значению заголовка "Content-Type"
        })
            .then(response => response.json()); // парсит JSON ответ в Javascript объект
    };

    getAuthCheckUser = async (login, pass) => {
        const res = await this.getResource(`/auth/get`, `login=${login}&pass=${pass}`);
        return res;
    };

    setNewUer = async (data) => {
        const res = await this.setDataResource('/auth/set', data);
        return res;
    };


}
