const updateCatalogPopularFilms = (state, action) => {

    if (state === undefined) {
        return {
            films: [],
            loading: true,
            error: null
        };
    }

    switch (action.type) {
        case 'FETCH_POPULAR_FILMS_REQUEST':
            return {
                films: [],
                loading: true,
                error: null
            };

        case 'FETCH_POPULAR_FILMS_SUCCESS':
            console.log('state', state);

            return {
                films: action.payload,
                loading: false,
                error: null
            };

        case 'FETCH_POPULAR_FILMS_FAILURE':
            return {
                films: [],
                loading: false,
                error: action.payload
            };

        default:
            return state.popularFilms;
    }
};

export default updateCatalogPopularFilms;
