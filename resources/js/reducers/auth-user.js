const updateAuthUser = (state, action) => {
    if (state === undefined) {
        return {
            data: [],
            wish: []
        }
    }
    switch (action.type) {
        case 'ADD_USER_CHECK_AUTH':
            return {
                data: action.payload
            };

        case 'REMOVE_USER_CHECK_AUTH':
            return state;

        default:
            return state.updateAuthUser;
    }

};

export default updateAuthUser;
