import updateCatalogPopularFilms from './catalog-popular-films';
import updateAuthUser from './auth-user';

const reducer = (state, action) => {
    return {
        popularFilms: updateCatalogPopularFilms(state, action),
        updateAuthUser: updateAuthUser(state,action)
    };
};

export default reducer;
