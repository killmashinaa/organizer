import withCatalogFilms from './with-catalog-films';
import withDatastoreService from "./with-datastore-service";

export {
    withCatalogFilms,
    withDatastoreService
};
