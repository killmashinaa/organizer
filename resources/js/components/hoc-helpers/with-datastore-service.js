import React from 'react';
import { DatastoreServiceConsumer } from '../datastore-service-provider';

const withDatastoreService = () => (Wrapped) => {

    return (props) => {
        return (
            <DatastoreServiceConsumer>
                {
                    (datastoreService) => {
                        return (<Wrapped {...props}
                                         datastoreService={datastoreService}/>);
                    }
                }
            </DatastoreServiceConsumer>
        );
    }
};

export default withDatastoreService;
