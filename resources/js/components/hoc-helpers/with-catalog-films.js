import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Spinner from '../elements/spinner/spinner';
import FilmsCards from '../elements/films-cards/films-cards';

const withCatalogFilms = (WrappedComponent, selectData) => {
        return class extends Component {
            constructor(props) {
                super(props);
            }

            state = {
                Films: [],
                loading: true,
                endLoadCatalog: false
            };

            componentDidMount() {
                this.updateFilms();
            }

            componentDidUpdate(prevProps, prevState, snapshot) {
                if (!this.state.endLoadCatalog) {
                    document.addEventListener('scroll', this.trackScrolling);
                }
            }

            componentWillUnmount() {
                document.removeEventListener('scroll', this.trackScrolling);
            }

            isBottom(el) {
                return el.getBoundingClientRect().bottom - 10 <= window.innerHeight;
            }

            trackScrolling = () => {
                const wrappedElement = document.getElementById('root');
                if (this.isBottom(wrappedElement)) {
                    this.setState({loading: true});
                    const Films = this.state.Films;
                    const thisPage = Films.page;
                    const maxPage = Films.total_pages;
                    const nextPage = thisPage + 1;
                    document.removeEventListener('scroll', this.trackScrolling);
                    if (nextPage <= maxPage) {
                        this.updateFilms(nextPage);
                    } else {
                        this.setState({loading: false, endLoadCatalog: true});
                    }
                }
            };

            updateFilms(page = 1) {
                selectData(page, this.state.Films).then(this.onFilmsLoaded)
            }

            onFilmsLoaded = (films) => {
                this.setState({Films: films, loading: false});
            };


            render() {
                const {Films, loading } = this.state;
                let itemsResult = Films.results;
                if (!itemsResult) {
                    itemsResult = [];
                }

                const FilmsTag = itemsResult.map((item) => {
                    return (
                        <FilmsCards key={item.id} item={item} />
                    );
                });
                const spinner = loading ? <Spinner size={80} /> : null;
                return (
                    <WrappedComponent {...this.props.data}>
                        <br/>
                        <Grid container spacing={3} >
                            {FilmsTag}
                        </Grid>
                        {spinner}
                        {/*<button onClick={event => this.handleScroll(event)}>Click</button>*/}
                    </WrappedComponent>
                )
            }

        }
};

export default withCatalogFilms;
