import {
    DatastoreServiceProvider,
    DatastoreServiceConsumer
} from './datastore-service-provider';

export {
    DatastoreServiceProvider,
    DatastoreServiceConsumer
};
