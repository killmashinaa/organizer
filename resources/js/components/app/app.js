import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';

import Header from '../header';
import {DataPage, PopularFilmsPage, NowPlayingFilmsPage, UpcomingFilmsPage, RegisterPage} from '../pages';
import ScrollTop from '../elements/scroll-top/scroll-top';


import './app.css';

const App = () => {
    return (
        <Box>
            <Header/>
            <Container maxWidth="lg">
            <Switch>
                <Route
                    path="/"
                    component={DataPage}
                    exact/>
                <Route
                    path="/category/popular"
                    component={PopularFilmsPage}
                    />
                <Route
                    path="/category/now_playing"
                    component={NowPlayingFilmsPage}
                />
                <Route
                    path="/category/upcoming"
                    component={UpcomingFilmsPage}
                />
                <Route
                    path="/register"
                    component={RegisterPage}
                />
            </Switch>
                <ScrollTop/>
            </Container>
        </Box>
    );
};

export default App;
