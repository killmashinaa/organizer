import IconButton from "@material-ui/core/IconButton";
import AccountCircle from "@material-ui/core/SvgIcon/SvgIcon";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import React  from 'react';
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import DialogAuth from "../dialogs/dilalog-auth";
import {Link} from "react-router-dom";
import {makeStyles} from "@material-ui/core";
import {useSelector, useDispatch } from "react-redux";




const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },

    list: {
        width: 250,
    },
    fullList: {
        width: 'auto',
    },
    link: {
        margin: theme.spacing(1),
        textDecoration: 'none',
        color: 'black'
    },

    linkAuth: {
        margin: theme.spacing(1),
        textDecoration: 'none',
        color: 'white'
    },
}));

