import React, { useState, useEffect }  from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import { Link } from 'react-router-dom';
import DialogAuth from '../../elements/dialogs/dilalog-auth';
import {useSelector, useDispatch } from "react-redux";


const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },

    list: {
        width: 250,
    },
    fullList: {
        width: 'auto',
    },
    link: {
        margin: theme.spacing(1),
        textDecoration: 'none',
        color: 'black'
    },

    linkAuth: {
        margin: theme.spacing(1),
        textDecoration: 'none',
        color: 'white'
    },
}));

const AuthElement = () => {
    // const dispatch = useDispatch();
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [openDialogAuth, setOpenDialogAuth] = React.useState(false);
    const open = Boolean(anchorEl);

    const authDataUser =  useSelector(state => state.updateAuthUser.data);
    function handleMenu(event) {
        setAnchorEl(event.currentTarget);
    }

    function handleClose() {
        setAnchorEl(null);
    }

    function handleClickOpenDialogAuth() {
        setOpenDialogAuth(true);
    }

    function handleCloseDialogAuth() {
        setOpenDialogAuth(false);
    }

    const AuthCheck = () => {
        return (
            <div>
                <IconButton
                    aria-label="Account of current user"
                    aria-controls="menu-appbar"
                    aria-haspopup="true"
                    onClick={handleMenu}
                    color="inherit"
                >
                    <AccountCircle />
                </IconButton>
                <Menu
                    id="menu-appbar"
                    anchorEl={anchorEl}
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                    keepMounted
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                    open={open}
                    onClose={handleClose}
                >
                    <MenuItem onClick={handleClose}>Profile</MenuItem>
                    <MenuItem onClick={handleClose}>My account</MenuItem>
                </Menu>
            </div>
        )
    };

    const AuthNoCheck = () => {
        return (
            <React.Fragment>
                <div className={classes.linkAuth}  >
                    <ListItem button key="Auth" onClick={handleClickOpenDialogAuth}>
                        <ListItemText primary="Войти" />
                    </ListItem>
                    <DialogAuth open={openDialogAuth} close={() => {handleCloseDialogAuth()}}/>
                </div>
                <Link variant="inherit" to="/register" className={classes.linkAuth}>
                    <ListItem button key="Register">
                        <ListItemText primary="Регистрация" />
                    </ListItem>
                </Link>
            </React.Fragment>
        )
    };


    let AuthRender;
    if(authDataUser.length !== 0) {
        AuthRender = AuthCheck;
    } else {
        AuthRender = AuthNoCheck;
    }

    return (
        <React.Fragment>
            <AuthRender/>
        </React.Fragment>
    );
};


export default function AppBarHeader() {
    const classes = useStyles();
    const [state, setState] = React.useState(false);
    const [auth, setAuth] = React.useState(false);
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [openDialogAuth, setOpenDialogAuth] = React.useState(false);
    const open = Boolean(anchorEl);



    const toggleDrawer = (side, open) => event => {
        if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        setState({ ...state, [side]: open });
    };

    function handleChange(event) {
        setAuth(event.target.checked);
    }






    const sideList = side => (
        <div
            className={classes.list}
            role="presentation"
            onClick={toggleDrawer(side, false)}
            onKeyDown={toggleDrawer(side, false)}
        >
            <List>
                <Link variant="inherit" to="/" className={classes.link}>
                    <ListItem button key="Home">
                        <ListItemIcon><InboxIcon /></ListItemIcon>
                        <ListItemText primary="Главная" />
                    </ListItem>
                </Link>
                <Link variant="inherit" to="/category/popular" className={classes.link}>
                    <ListItem button key="Popular">
                        <ListItemIcon><InboxIcon /></ListItemIcon>
                        <ListItemText primary="Популярные" />
                    </ListItem>
                </Link>
                <Link variant="inherit" to="/category/now_playing" className={classes.link}>
                    <ListItem button key="Now playing">
                        <ListItemIcon><InboxIcon /></ListItemIcon>
                        <ListItemText primary="Сейчас в прокате" />
                    </ListItem>
                </Link>
                <Link variant="inherit" to="/category/upcoming" className={classes.link}>
                    <ListItem button key="Upcoming">
                        <ListItemIcon><InboxIcon /></ListItemIcon>
                        <ListItemText primary="Скоро в прокате" />
                    </ListItem>
                </Link>
            </List>
            <Divider />
        </div>
    );

    return (
        <div className={classes.root}>
            <FormGroup>
                <FormControlLabel
                    control={<Switch checked={auth} onChange={handleChange} aria-label="LoginSwitch" />}
                    label={auth ? 'Logout' : 'Login'}
                />
            </FormGroup>
            <AppBar position="static">
                <Toolbar>
                    <IconButton onClick={toggleDrawer('left', true)} edge="start" className={classes.menuButton} color="inherit" aria-label="Menu">
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" className={classes.title}>
                        MovieDB
                    </Typography>
                <AuthElement/>
                </Toolbar>
            </AppBar>
            <SwipeableDrawer
                open={state.left ? state.left : false}
                onClose={toggleDrawer('left', false)}
                onOpen={toggleDrawer('left', true)}
            >
                {sideList('left')}
            </SwipeableDrawer>
        </div>
    );
}
