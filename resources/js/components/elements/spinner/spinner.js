import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles(theme => ({
    progress: {
        margin: theme.spacing(2),
    },
}));

export default function CircularIndeterminate(props) {
    const classes = useStyles();
    const style = {
        textAlign: "center"
    };

    let size = props.size;

    if (size === undefined) {
        size = 80;
    }

    return (
        <div style={style}>
            <CircularProgress className={classes.progress} color="secondary" size={size}/>
        </div>
    );
}
