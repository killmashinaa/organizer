import React from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Zoom from '@material-ui/core/Zoom';
import Fab from '@material-ui/core/Fab';
import UpIcon from '@material-ui/icons/KeyboardArrowUp';
import {blue, green, lightBlue} from '@material-ui/core/colors';

import './scroll-top.css';




export default function scrollTop() {

    const useStyles = makeStyles(theme => ({
        fab: {
            position: 'absolute',
            bottom: theme.spacing(2),
            right: theme.spacing(2),
        },
        fabGreen: {
            color: theme.palette.common.white,
            backgroundColor: blue[500],
            '&:hover': {
                backgroundColor: blue[600],
            },
        },
    }));
    const classes = useStyles();
    const fabs =
        {
            color: 'inherit',
            className: clsx(classes.fab, classes.fabGreen),
            icon: <UpIcon />,
            label: 'Expand',
        };

    return (
            <Fab
                onClick={() => {window.scroll(0,0)}}
                aria-label={fabs.label}
                className={fabs.className + ' botton-top-scroll'}
                color={fabs.color}
                style={{ position: "fixed", scrollBehavior: 'smooth'}}>
                {fabs.icon}
            </Fab>
    );
}
