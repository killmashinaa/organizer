import React, { useCallback } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import DatastoreAuth from "../../../services/datastore-auth";
import { useDispatch } from 'react-redux';
import {addUserCheckAuth} from '../../../actions'



export default function DialogAuth(props) {
    const dispatch = useDispatch();
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm')); // to full screen  dialog
    const [login, setLogin] = React.useState({email: ''});
    const [pass, setPass] = React.useState({pass: ''});
    const [authData, setAuthData] = React.useState(false);

    const checkAuth = (evt) => {
        evt.preventDefault();
      const dataStoreAuth = new DatastoreAuth;
      const data = {...login, ...pass};
      dataStoreAuth.getAuthCheckUser(data.email, data.pass).then((res) => {
          setAuthData(res);
      });

    };

    if (authData) {
        dispatch(addUserCheckAuth(authData));
        return (
            <div>
                <Dialog open={props.open} onClose={props.close} aria-labelledby="form-dialog-title" >
                    <DialogTitle id="form-dialog-title">Авторизация</DialogTitle>
                    <form onSubmit={checkAuth}>
                        <DialogContent>
                            <DialogContentText>
                                Здравствуй {authData.name} !
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={props.close} color="primary">
                                Cancel
                            </Button>
                            <Button onClick={() => {setAuthData(false)}} color="primary">
                                Out
                            </Button>
                            <Button color="primary">
                                Account
                            </Button>
                            {/*<Button onClick={() => {}} type="submit" color="primary">*/}
                            {/*    Submit*/}
                            {/*</Button>*/}
                        </DialogActions>
                    </form>
                </Dialog>
            </div>
        );
    } else {

        return (
            <div>
                <Dialog open={props.open} onClose={props.close} aria-labelledby="form-dialog-title" >
                    <DialogTitle id="form-dialog-title">Авторизация</DialogTitle>
                    <form onSubmit={checkAuth}>
                        <DialogContent>
                            <DialogContentText>
                                Здравствуйте!
                            </DialogContentText>
                            <TextField
                                required
                                autoFocus
                                margin="dense"
                                id="email"
                                label="Ваш Email"
                                type="email"
                                fullWidth
                                onChange={event => setLogin({email: event.target.value})}
                            />
                            <TextField
                                required
                                margin="dense"
                                id="password"
                                label="Пароль"
                                autoComplete="current-password"
                                type="password"
                                fullWidth
                                onChange={event => setPass({pass: event.target.value})}
                            />
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={props.close} color="primary">
                                Cancel
                            </Button>
                            <Button onClick={() => {}} type="submit" color="primary">
                                Submit
                            </Button>
                        </DialogActions>
                    </form>
                </Dialog>
            </div>
        );

    }

}
