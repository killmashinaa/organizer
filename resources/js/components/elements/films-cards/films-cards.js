import React from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import DatastoreService from '../../../services/datastore-service';
import Spinner from '../spinner/spinner';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    card: {
        maxWidth: 345,
        padding: theme.spacing(1),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    avatar: {
        backgroundColor: red[500],
    },
    videoCollapse: {
        padding: 0
    }
}));

export default function FilmsCards(props) {
    const {item} = props;
    const classes = useStyles();
    const [expanded, setExpanded] = React.useState(false);
    const [videoForFilms, setVideoForFilms] = React.useState([]);
    const [loading, setLoading] = React.useState(true);

    function handleExpandClick() {
        setExpanded(!expanded);
        getVideoForFilms(item.id);
    }

    function getVideoForFilms(id) {
        const datastoreService = new DatastoreService;
        datastoreService.getVideoForFilm(id).then((value) => {

            const youtubeTags = value.map((item) => {
                return (
                    <div key={item.id}>
                    <iframe width="100%" height="177" src={item.video_url} frameBorder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowFullScreen />
                    </div>
                );
            });

            setVideoForFilms(youtubeTags);
            setLoading(false);
        });
    }


    const spinner = loading ? <Spinner size={40} /> : null;
    const emptyVideo = loading ? null : 'Нет трейлеров';
    const videoFilms = videoForFilms.length>0 ? videoForFilms : emptyVideo;
    return (
        <Grid item xs={12} sm={4}>
        <Card className={classes.card}>
            <CardHeader
                avatar={
                    <Avatar aria-label="Recipe" className={classes.avatar}>
                        {item.title[0]}
                    </Avatar>
                }
                action={
                    <IconButton aria-label="Settings">
                        <MoreVertIcon />
                    </IconButton>
                }
                title={item.title}
                subheader={item.release_date}
            />
            <CardMedia
                className={classes.media}
                image={item.backdrop_path}
                title="Paella dish"
            />
            <CardContent>
                <Typography variant="body2" color="textSecondary" component="p">
                    {item.overview}
                </Typography>
            </CardContent>
            <CardActions disableSpacing>
                <IconButton aria-label="Add to favorites">
                    <FavoriteIcon />
                </IconButton>
                <IconButton aria-label="Share">
                    <ShareIcon />
                </IconButton>
                <IconButton
                    className={clsx(classes.expand, {
                        [classes.expandOpen]: expanded,
                    })}
                    onClick={handleExpandClick}
                    aria-expanded={expanded}
                    aria-label="Show more"
                >
                    <ExpandMoreIcon />
                </IconButton>
            </CardActions>
            <Collapse in={expanded} timeout="auto" unmountOnExit>
                <CardContent>
                    {/*<Typography paragraph>Method:</Typography>*/}
                    {spinner}
                    {videoFilms}

                </CardContent>
            </Collapse>
        </Card>
        </Grid>
    );
}
