import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';


const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
    dense: {
        marginTop: 16,
    },
    menu: {
        width: 200,
    },
    button: {
        margin: theme.spacing(1),
    },
    input: {
        display: 'none',
    },
}));

export default function RegisterPage() {
    const [state, setState] = React.useState({
        checkedA: true,
        checkedB: true,
    });
    const classes = useStyles();

    const handleChange = name => event => {
        setState({ ...state, [name]: event.target.checked });
    };

    return (
        <Grid container
              direction="row"
              justify="center"
              alignItems="center">
            <Grid item sm={3}>
                <form className={classes.container} noValidate autoComplete="on">
                    <TextField
                        id="filled-name"
                        label="Имя"
                        className={classes.textField}
                        margin="normal"
                        variant="outlined"
                    />
                    <TextField
                        id="outlined-email-input"
                        label="Email"
                        className={classes.textField}
                        type="email"
                        name="email"
                        autoComplete="email"
                        margin="normal"
                        variant="outlined"
                    />
                    <TextField
                        id="outlined-password-input"
                        label="Пароль"
                        className={classes.textField}
                        type="password"
                        autoComplete="current-password"
                        margin="normal"
                        variant="outlined"
                    />

                    <TextField
                        id="outlined-password-input"
                        label="Пароль"
                        className={classes.textField}
                        type="password"
                        autoComplete="current-password"
                        margin="normal"
                        variant="outlined"
                    />

                    <FormControlLabel
                        control={
                            <Switch checked={state.checkedA} onChange={handleChange('checkedA')} value="checkedA" />
                        }
                        label="Соглашаюсь с политикой конфиденциальности"
                    />

                    <Button variant="contained" color="primary" className={classes.button}>
                        Зарегистироваться
                    </Button>

                </form>
            </Grid>
        </Grid>
    );
}
