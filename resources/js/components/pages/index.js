import DataPage from './data-pages';
import PopularFilmsPage from './popular-films-page';
import NowPlayingFilmsPage from './now-playing-films-page';
import UpcomingFilmsPage from './upcoming-films-page';
import RegisterPage from './register-page';


export {
  DataPage,
  PopularFilmsPage,
  NowPlayingFilmsPage,
  UpcomingFilmsPage,
    RegisterPage
};
