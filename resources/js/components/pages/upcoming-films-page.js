import React, { Component } from 'react';
import DatastoreService from '../../services/datastore-service';
import {withCatalogFilms} from '../hoc-helpers';

const datastoreService = new DatastoreService;
const Template = (props) => {
    return (
        <div {...props} >
            {props.children}
        </div>
    );
};

const getData = datastoreService.getUpcomingFilms;
const UpcomingFilmsWithCatalogFilms = withCatalogFilms(Template, getData);
const props = {
    className: 'upcoming catalog',
    id: 'upcoming-catalog'
};
export default function UpcomingFilmsPage() {
    return (
        <div>
            <h2>Скоро в прокате</h2>
            <UpcomingFilmsWithCatalogFilms data={props}/>
        </div>
    )
}
