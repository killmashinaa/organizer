import React, { Component } from 'react';
import {compose, bindActionCreators} from "redux";
import { connect } from 'react-redux';
import DatastoreService from '../../services/datastore-service';
import {withCatalogFilms} from '../hoc-helpers';
import {withDatastoreService} from '../hoc-helpers/';
import {fetchPopularFilms} from '../../actions'

const Template = (props) => {
    return (
        <div {...props} >
            {props.children}
        </div>
    );
};

class NowPlayingFilmsPage extends Component {

    componentDidMount() {
        this.props.fetchPopularFilms();

        // setTimeout(() => {
        //     this.props.fetchPopularFilms();
        //
        // }, 2000);
    }


    render() {
        const { films, loading, error } = this.props;

        const datastoreService = new DatastoreService();
        const getData = datastoreService.getPopularFilmsAsync;
        const NowPlayingFilmsWithCatalogFilms = withCatalogFilms(Template, getData);
        const props = {
            className: 'popular-films catalog',
            id: 'popular-films-catalog'
        };

        return (
            <div>
                <h2>Популярные фильмы</h2>
                <NowPlayingFilmsWithCatalogFilms data={props}/>
            </div>
        )
    }
}

const mapStateToProps = ({popularFilms: {films, loading, error }}) => {
    return {films, loading, error };
};

const mapDispatchToProps = (dispatch, { datastoreService }) => {
    return bindActionCreators({
        fetchPopularFilms: fetchPopularFilms(datastoreService),
    }, dispatch);
};

export default compose(
    withDatastoreService(),
    connect(mapStateToProps, mapDispatchToProps)
)(NowPlayingFilmsPage);
