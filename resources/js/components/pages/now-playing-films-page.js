import React, { Component } from 'react';
import DatastoreService from '../../services/datastore-service';
import {withCatalogFilms} from '../hoc-helpers';

const datastoreService = new DatastoreService;
const Template = (props) => {
    return (
        <div {...props} >
            {props.children}
        </div>
    );
};

const getData = datastoreService.getNowPlayingFilms;
const NowPlayingFilmsWithCatalogFilms = withCatalogFilms(Template, getData);
const props = {
    className: 'now-playing catalog',
    id: 'now-playing-catalog'
};
export default function NowPlayingFilmsPage() {
    return (
        <div>
            <h2>Сейчас в прокате</h2>
            <NowPlayingFilmsWithCatalogFilms data={props}/>
        </div>
    )
}
