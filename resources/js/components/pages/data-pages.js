import React, { Component } from 'react';
import DatastoreService from '../../services/datastore-service';
import Spinner from '../elements/spinner/spinner';
import ImageGridList from '../elements/image-grid-list/image-grid-list';

export default class DataPage extends Component {

    datastoreService = new DatastoreService;

    state = {
        people: [],
        tileData: [],
        loading: true
    };

    componentDidMount() {
        this.updatePeople();
        this.updateTileData();
    }

    onPeopleLoaded = (people) => {
        this.setState({ people, loading: false });
    };

    onTileDataLoaded = (tileData) => {
        this.setState({tileData, loading: false})
    };


    updatePeople() {
        this.datastoreService
            .getAllFilmsWithJackReacher()
            .then(this.onPeopleLoaded);
    }

    updateTileData() {
        this.datastoreService
            .getImages()
            .then(this.onTileDataLoaded);

    }

    render() {
        const { people, loading, tileData } = this.state;
        const items = people.map((item) => {
            return (
                <p key={item.id}>{item.overview}</p>
            );
        });
        const spinner = loading ? <Spinner size={80} /> : null;
        return (
            <div>
                {spinner}
                <h2>New film!</h2>
                {items}
                <ImageGridList tileData={tileData} />
            </div>
        );
    }
}



